"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const env_1 = require("./env");
const mongoose_1 = __importDefault(require("mongoose"));
const app = express_1.default();
let http = require('http').Server(app);
const io = require('socket.io')(http);
let users = [];
let connections = [];
io.on('connection', () => {
    console.log(`new User connected`);
});
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});
mongoose_1.default.connect(env_1.environment.DB_CONNECT, { useUnifiedTopology: true, useNewUrlParser: true }, (err) => {
    console.log('ERROR', err);
    console.log("conected to database");
});
app.listen(env_1.environment.port, err => {
    if (err) {
        console.log("this>>>>>>: err", err);
    }
    return console.log(`listening to port ${env_1.environment.port}`);
});
//# sourceMappingURL=app.js.map