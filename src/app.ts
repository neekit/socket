import express from "express";
import { environment } from './env';
import mongoose from 'mongoose';


const app = express();
const socket = require('socket.io');

let users = [];
let connections = [];


app.get('/', (req: any, res: any) => {
    res.sendFile(__dirname + '/index.html')
})

// mongoose.connect(environment.DB_CONNECT, { useUnifiedTopology: true, useNewUrlParser: true }, (err) => {
//     console.log('ERROR', err);
//     console.log("conected to database");
// });

let server = app.listen(environment.port, err => {
    if (err) {
        console.log("this>>>>>>: err", err)
    }
    return console.log(`listening to port ${environment.port}`)
});

let io = socket(server);

io.on('connection', (socket) => {
    console.log('connection created', socket.id);
    socket.on('chat', (data) => {
        io.sockets.emit('chat',data)
    })
})

